Fala aí pessoal blz? 
Agradeço por terem vindo baixar o script e você pode modificar ele vontade.
Volte sempre!

## O objetivo

Porque compartilhar o script?

1. Você pode precisar todos os arquivos contidos nele e facilitar instalações no(s) seu(s) Pc(s) sem ficar indo nos sites baixar as mesmas coisas que eu.
2. Pode sugerir modificações/inclusões para algum programa que você precise usar na sua infra.
3. Ele é gratuito e você pode modificar e compartilhar.
4. Fiz para otimizar o meu tempo na hora instalar os programas que uso no Ubuntu.

## Como posso retribuir?

1. Diga que baixou do meu repositório que ajuda a mim e outras pessoas.
2. Pode sugerir modificações.
3. Pode sugerir inclusão de algum programa ou tarefa a cerca do que está no script. 
4. Pode fazer fork e criar uma versão totalmente sua.

## Clone o repositório

1. Mantenha uma cópia com você e use quando precisar.
2. Acompanhe o desenvolvimento dele.
3. Existe um plano para rodar o DaVinci Resolve em distros que ele ainda não roda e facilitar sua vida.