#!/bin/bash

sudo rm /var/lib/dpkg/lock-frontend ; sudo rm /var/cache/apt/archives/lock ;

# Adicionar suporte a i386 #

sudo dpkg --add-architecture i386 && sudo apt update 

# Instalar gcc make dkms e flatpak pra loja #
sudo apt-get install make dkms gcc flatpak gnome-software-plugin-flatpak -y


## Adicionar repositório Flathub ##

sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

## Instalar o repositório do OpenRazer para dispositivos Razer ##

#sudo add-apt-repository ppa:openrazer/stable

# Adicionar repositório do Lutris

sudo add-apt-repository ppa:lutris-team/lutris
sudo apt update 

## Atualização do sistema ##

sudo apt update && sudo apt dist-upgrade -y && sudo apt autoclean -y && sudo apt autoremove -y &&

wget -c https://dl.winehq.org/wine-builds/winehq.key
wget -c https://dl.google.com/linux/linux_signing_key.pub
wget -c https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
wget -c https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64

sudo apt-get -y install snapd ubuntu-restricted-extras winff guvcview lutris libvulkan1 libvulkan1:i386

## Rodando update ##

sudo apt-get update

# Chave do Wine #

sudo apt-key add winehq.key

# Cahve do Chormium e Chrome #

sudo apt-key add linux_signing_key.pub

## Instalando os arquivos baixados .deb ##

sudo dpkg -i *.deb

# Instalar o Wine # 

sudo apt-get install --install-recommends winehq-stable wine-stable wine-stable-i386 wine-stable-amd64 -y

## Instalando Apps do Flathub ##

sudo flatpak install flathub com.sublimetext.three -y &&
sudo flatpak install flathub org.gnome.Evolution -y &&
sudo flatpak install flathub com.transmissionbt.Transmission -y &&
sudo flatpak install flathub io.github.liberodark.OpenDrive -y &&
sudo flatpak install flathub org.videolan.VLC -y &&
sudo flatpak install flathub org.kde.kdenlive -y &&
sudo flatpak install flathub org.libreoffice.LibreOffice -y &&
sudo flatpak install flathub com.valvesoftware.Steam -y &&
sudo flatpak install flathub com.spotify.Client -y &&
sudo flatpak install flathub com.gitlab.davem.ClamTk -y &&
# sudo flatpak install flathub xyz.z3ntu.razergenie -y &&
sudo flatpak install flathub org.filezillaproject.Filezilla -y &&
sudo flatpak install flathub org.gnome.Rhythmbox3 -y &&
sudo flatpak install flathub org.gimp.GIMP -y &&
sudo flatpak install flathub com.dropbox.Client -y &&
sudo flatpak install flathub org.darktable.Darktable -y &&
sudo flatpak install flathub com.discordapp.Discord -y &&
sudo flatpak install flathub org.gnome.gedit -y &&
sudo flatpak install flathub im.pidgin.Pidgin -y &&
sudo flatpak install flathub org.gnome.Tomboy -y &&
sudo flatpak install flathub com.github.unrud.VideoDownloader -y &&
sudo flatpak install flathub org.gnome.Totem -y &&
sudo flatpak install flathub org.inkscape.Inkscape -y &&
sudo flatpak install flathub com.vinszent.GnomeTwitch -y &&
sudo flatpak install flathub org.gnome.gitg -y &&
sudo flatpak install flathub org.gnome.Shotwell -y &&
sudo flatpak install flathub com.github.unrud.djpdf -y &&
sudo flatpak install flathub org.gnome.DejaDup &&
sudo flatpak install flathub org.apache.netbeans -y &&

## Instala o Daemon do OpenRazer

#sudo apt-get -y install openrazer-meta &&

# Atualizar os faltpaks e instalar os faltantes # 

sudo flatpak update -y

# Adicionar usuário do grupo plugdev
sudo apt-get -y update && sudo apt-get -y upgrade && sudo apt-get -f install
sudo gpasswd -a rodrigo plugdev

# Instalando o banco de dados #

sudo apt-get update 
sudo apt-get -y install postgresql postgresql-client postgresql-contrib pgadmin3
sudo service postgresql start
sudo systemctl enable postgresql.service

# Limpar e remover os pacotes não necessários #

sudo apt autoclean -y && sudo apt autoremove -y

echo "Fim"
